<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>">
	<head>
		<title><?php print $head_title ?></title>
		<?php print $head ?>
		<?php print $styles ?>
		<?php print $scripts ?>
		<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
	</head>
	
	<body>
		<div id="menu">
			<?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist')) ?><?php } ?>
		</div>
		<!-- end #menu -->

		<div id="header">
			<?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
			<h2 class='site-slogan'><?php if ($site_slogan) { print $site_slogan; } ?></h2>
		</div>
		<!-- end #header -->

		<div id="wrapper">
			<div id="content-pane">
				<div id="pane">
					<?php if ($mission) { ?>
					<div id="mission">
						<h2 class="title">Welcome to <?php print $site_name ?>!</h2>
						<div class="mission-content"><?php print $mission ?></div>
					</div>
					<?php } ?>
	
					<div id="main">
						<?php print $breadcrumb ?>
						<div class="tabs"><?php print $tabs ?></div>
						<?php print $help ?>
						<?php print $messages ?>
						<h1 class="title"><?php print $title ?></h1>
						<?php print $content; ?>
						<?php print $feed_icons; ?>
					</div>
					<!-- end #main -->
	
				</div>
				<!-- end #pane -->
	
				<div id="sidebar">
					<?php print $left ?>
					<?php print $right ?>
				</div>
				<!-- end #links -->
	
				<div style="clear: both;">&nbsp;</div>
			</div>
			<!-- end #content -->
		</div>
		<div id="footer">
			<p id="legal">Copyright &copy; <?php print date('Y')." ".$site_name ?>. <a href="http://www.freecsstemplates.org/" target="_blank">Designer</a>. Ported by <a href="http://drupal.org/user/39343" target="_blank">Jason Swaby</a></p>
			<p id="brand"><?php print $site_name ?></p>
			<?php print $footer ?>
			<?php print $footer_message ?>
		</div>
		<!-- end #footer -->
		<?php print $closure ?>
	</body>
</html>
